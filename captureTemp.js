//温度計測関連
var serialport = require('serialport');
var numCaputure = 10;//計測回数 0.1秒に一回センサーから値が来るはず
var cntCaputure = 0;
var arTemp = [];
var limitTemp = 25;//32℃を超えていたら、アラート

//通知関連
var request = require('request');
var api_token = 'o.pcEyv20zIR4RPmlVEKVzcTWhF8tPmlar';
var api_url = 'https://api.pushbullet.com/v2/pushes';

// Serial Port
var portName = '/dev/ttyACM0';
var sp = new serialport.SerialPort(portName, {
    baudRate: 9600,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false,
    parser: serialport.parsers.readline("\n")   // ※修正：パースの単位を改行で行う
});

console.log("計測開始");

// data from Serial port
sp.on('data', function(input) {
    var buffer = new Buffer(input, 'utf8');
    var jsonData;
    try {
        var temp = parseFloat(JSON.parse(buffer));
        if(Math.abs(temp - arTemp[arTemp.length-1]) > 10){//前回検測値と10℃以上違う場合、異常値とみなす
            return;
        }
        arTemp.push(temp);
        cntCaputure++;
        console.log("計測中 "+cntCaputure+" "+temp);
        if(cntCaputure == numCaputure){
            if(arTemp.length == 1){//初回取得値が異常だった場合、再取得
                cntCaputure = 0;
                return;
            }
            var sum = 0;
            for (var i = arTemp.length - 1; i >= 0; i--) {
                sum += arTemp[i];
            }
            var avg = sum / numCaputure;
            console.log("取得結果："+avg);
            if(avg > limitTemp){
                noticeHot(avg);
            }
            console.log("計測完了");
        }
    } catch(e) {
        // データ受信がおかしい場合無視する
        return;
    }
});

function noticeHot(crtTemp){
    console.log("Hot!!!");
    var options = {
        url:api_url,
        headers: {
            'Access-Token': api_token,
            'Content-Type': 'application/json'
        },
        form: {
            type : 'note',
            title: '室温アラート',
            body: limitTemp+'℃を超えています。現在の室温は'+crtTemp+'℃です。'
        }
    };
    request.post(options, function(err, res, body) {
        if (!err && res.statusCode === 200) {
            console.log(body);
        }else{
            console.log('API実行失敗');
            console.log(res);
        }
        process.exit(0);
    });
}

