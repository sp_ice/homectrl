#!/bin/bash
# /etc/init.d/homeCtrl
# description: homeCtrl stop:stop Script

start() {
    echo -n "Starting homeCtrl: "
    /home/pi/.nvm/versions/node/v6.2.0/bin/forever start -a --uid homeCtrl /home/pi/homeCtrl/bin/www
    return 0
}
stop() {
    /home/pi/.nvm/versions/node/v6.2.0/bin/forever stop homeCtrl
    return 0
}
case "$1" in
  start)
    stop
    start
    ;;
  restart)
    stop
    start
    ;;
  stop)
    stop
    ;;
   *)
    echo $"Usage: $0 {start|stop|restart}"
    exit 2

esac